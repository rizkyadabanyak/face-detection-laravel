@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{route('home_submit')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Url</label>
                                <input type="text" class="form-control" name="url" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Url">
                            </div>
                            {{--                            @foreach($data as $a)--}}
                            {{--                                {{$a->url}}--}}
                            {{--                            @endforeach--}}
                            <img src="{{$data->url}}" alt="" width="400">
                            <button type="submit" class="btn btn-primary">Cari</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
