<?php

namespace App\Http\Controllers;

use App\Models\Face;
use Illuminate\Http\Request;
use Clarifai\API\ClarifaiClient;
use Clarifai\DTOs\Inputs\ClarifaiURLImage;
use Clarifai\DTOs\Outputs\ClarifaiOutput;
use Clarifai\DTOs\Predictions\Concept;
use Clarifai\DTOs\Searches\SearchBy;
use Clarifai\DTOs\Searches\SearchInputsResult;
use Clarifai\DTOs\Models\ModelType;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Face::find(5);


        view()->share([
            'data' => $data,
        ]);

        return view('home');
    }
    public function indexSubmit(Request $request){
        $data = new Face();

        $data->url = $request->url;

        $data->save();
        return redirect()->ur('home');
    }
    public function face($id){
        $data = Face::find($id);

        $client = new ClarifaiClient('609300a4c2184ca690131b39989f24ec');

        $model = $client->publicModels()->generalModel();

        $input = new ClarifaiURLImage($data->url);

        $response = $model->batchPredict([
            new ClarifaiURLImage($data->url)
        ])->executeSync();

        $outputs = $response->get();

        foreach ($outputs as $output) {
            /** @var ClarifaiURLImage $image */
            $image = $output->input();
            echo "Predicted concepts for image at url " . $image->url() . "\n";

            /** @var Concept $concept */
            foreach ($output->data() as $concept) {
                echo $concept->name() . ': ' . $concept->value() . "\n";
            }
            echo "\n";
        }


        view()->share([
            'data' => $data,
        ]);

        return view('face');
    }
}
